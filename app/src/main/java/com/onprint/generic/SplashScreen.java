package com.onprint.generic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.onprint.sdk.MainScreen;
import com.onprint.sdk.SDKLayouts;
import com.onprint.ws.ONprint;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        init_ONprint_settings();
    }

    public void on_print(View view) {
        Intent main_view = new Intent(SplashScreen.this, MainScreen.class);
        startActivityForResult(main_view, 0);
    }

    private void init_ONprint_settings() {
        init_DB(); // Init ONprint Data Base

        SDKLayouts.no_splash_screen(); // Set no splash screen
        SDKLayouts.no_first_start_tuto(); // Set no tuorial launched at first start

        // To get your API key, please contact: mobile@onprint.com
        final String API_KEY = "INSERT YOUR API KEY HERE";

        // Init ONprint API with your API key
        ONprint.init(this, API_KEY);
    }

    private void init_DB() {
        Realm.init(getApplicationContext());
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("database.realm")
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }
}