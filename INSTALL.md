# GUI SDK Installation Guide


Here are the steps to use the GUI SDK.

## Build Installation

###  Add Light and GUI SDK packages

First download [Android light SDK][1] and [Android GUI SDK][1] from the developer resources page,
then put them inside your root project and unzip them.

Inside "settings.gradle" add:
``` java
'your_project', ':ONprint_Light', ':ONprint_GUI'
```

###  Gradle intitialization


Then add inside your main build.gradle:

``` java
dependencies {
    classpath 'com.android.tools.build:gradle:3.2.0'
    classpath "io.realm:realm-gradle-plugin:5.7.0"
    }
}
```

Then add inside your application/build.gradle:
``` java
apply plugin: 'realm-android'

compileOptions {
    sourceCompatibility JavaVersion.VERSION_1_8
    targetCompatibility JavaVersion.VERSION_1_8
}

dependencies {
    ...
    implementation 'com.android.support:design:28.0.0'
    implementation 'com.github.bumptech.glide:glide:4.1.1'
    implementation 'com.github.clans:fab:1.6.4'
    implementation 'com.ritesh:ratiolayout:1.0.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.4.0'
    implementation 'com.squareup.okhttp3:okhttp:3.11.0'
    implementation 'org.apache.commons:commons-text:1.6'
    implementation project(':ONprint_GUI')
    implementation project(':ONprint_Light')
}
```

### Manifest permissions and requirements

Add inside the AndroidManifest.xml the following permissions:
``` java
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.CAMERA" />


<uses-feature android:name="android.hardware.camera" android:required="true" />
<uses-feature android:name="android.hardware.camera.autofocus" />
<uses-feature android:name="android.hardware.telephony" android:required="false" />
```

Note: telephony requirement is important because without it
Google Play prevents tablet installation.

## Java Initialization

### Data Base initialization
``` java
Realm.init(getApplicationContext());
RealmConfiguration config = new RealmConfiguration.Builder()
    .name("database.realm")
    .schemaVersion(1)
    .deleteRealmIfMigrationNeeded()
    .build();
Realm.setDefaultConfiguration(config);
```

### SDK intialization

To init ONprint SDK you need a apiKey which is a character chain allowing you to use ONprint API services with your dedicating data.

``` java
ONprint.init(context, apiKey);
```

To obtain your personal API Key, please contact mobile@onprint.com


### Launch camera

Final step is to launch main ONprint activity:

``` java
Intent camera_view = new Intent(SplashScreen.this, MainScreen.class);
startActivity(camera_view);
```

## Dev

Now it's time... to code!
Please follow -> the [developer guide][3]


[1]: https://gitlab.com/onprint_public/Android_ONprint_GUI_SDK_Sample/tree/master/ONprintWS
[2]: https://gitlab.com/onprint_public/Android_ONprint_GUI_SDK_Sample/tree/master/OnprintSDK
[3]: https://gitlab.com/onprint_public/Android_ONprint_GUI_SDK_Sample/tree/master/DEV.md
