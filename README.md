# Android ONprint SDK Light Sample

In no time, create an application to get information of enriched images by taking picture.

## Introduction

This [Sample API][1] shows you how to use basic functionalities of ONprint GUI SDK.
By using [ONprint API][2] and [ONprint Light SDK][3], it provides you an interface for individual camera
connected to an iOS device in order
to get informations from picture.

GUI will use ONprint interface used especially in [ONprint Generic Application][4].
So if you flash an enriched image, associated image actions will poped up!
An historic is also available.

Furthermore, it allows you to record usage stats.

## Pre-requisites

- Android SDK 21
- Android Build Tools v27.0.3
- Android Support Repository
- Java 1.8

## Screenshots

<img src="screenshots/main.jpg" height="400" alt="Screenshot"/> 

## Getting Started with the Sample

This sample uses the Gradle build system. To build this project, run the SplashScreen class in Android Studio.


## SDK Integration

To start, install the SDK for your own project by following the [install doc][5].
<br/><br/>
Then, to integrate ONprint functions, please consult the [developer guide][6].


### Support

If you've found an error in this sample or if you have any question,
please send us an email to: mobile@onprint.com

### License

© 2019 by LTU Tech. All Rights Reserved.



[1]: https://gitlab.com/onprint_public/Android_ONprint_GUI_SDK_Sample
[2]: http://developer.onprint.com/api.html
[3]: https://gitlab.com/onprint_public/Android_ONprint_Light_SDK_Sample
[4]: https://play.google.com/store/apps/details?id=com.onprint.generic
[5]: https://gitlab.com/onprint_public/Android_ONprint_GUI_SDK_Sample/blob/master/INSTALL.md
[6]: https://gitlab.com/onprint_public/Android_ONprint_GUI_SDK_Sample/blob/master/DEV.md
