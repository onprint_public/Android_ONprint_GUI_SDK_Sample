# GUI SDK Developer Guide

Welcome to the ONprint developer guide.<br/><br/>
This document lists all the main ONprint GUI SDK functions to explain you how
to build an Android application using ONprint picture recognition.

## Fundamentals

All the basic function calls (like SDK init) are detailed in [light SDK developer guide][1].

## GUI functions

Here are the functions which are specifics to the SDK GUI:

### > Start ONprint Activity


To launch the camera view activity (the main screen) call the MainScreen class:
``` java
startActivityForResult(new Intent(Class.this, MainScreen.class), 0);
```

If you use "startActivity" your application will be stucked inside ONprint application.


### > CameraPreview class

The camera preview class allows you to customize your customized camera view class.
You need CameraPreview layout:

``` java
    <com.onprint.sdk.core.camera.CameraPreview
    android:id="@+id/camera_preview"
    android:layout_width="match_parent"
    android:layout_height="match_parent" />
```
and a cameraPreview object :
``` java
CameraPreview cameraPreview = findViewById(R.id.camera_preview);
```

### > Trigger a Recogition Action

You can trigger a picuter with takePicture() function. This function is also called when the button with ID R.id.buttonTakePicture is triggered:
``` java
cameraPreview.takePicture();
```


### > Active the camera flash light

To activate the camera flash light use this function with flash_actived set to true:
``` java
camera.set_flash(boolean flash_actived);
```

### > Release the camera

To release the camera use this function:
``` java
cameraPreview.releaseCamera();
```


## Image treatment

Parse image obtains from callback:

``` java
private void searchEnrichedImage(byte[] data) {
    new EnrichedImage(new ONprintBMP(view, data), "image/jpeg", new DeviceInformation().getSystemLanguage(),
    null, new IEnrichedImage() {
        @Override
        public void enriched(JSONObject object) {
            //Log.i(TAG, "JSON: " + object);
            ONprintEnrichedImage imageJson = new ONprintEnrichedImage(object);

            if (imageJson == null) {
                imageDetected = false;
                onScanCompleted(null);
                Log.e(TAG, "JSON is null");
                return;
            }

            imageDetected = true;
            onScanCompleted(imageJson);
        }

        @Override
        public void notEnriched() {
            onScanCompleted(null);
        }

        @Override
        public void failed(String error) {
            Log.e(TAG, error);
            onScanCompleted(null);
        }
    });
}
```

ONprintEnrichedImage has getters for each of the following attributes:

``` java
public class ONprintEnrichedImage extends RealmObject {
    @PrimaryKey
    protected long id;
    private String ONprintImageId;
    private String pathONprintImageFile;
    private String bigThumbnailUrl;
    private String languageCode;
    protected String title;
    private String documentId;
    protected String sessionId;
    protected String theme;
    private String dateOfScan;
    private String hourOfScan;
    private boolean isFavorite;
    private boolean isSent;
    private int order;
    private boolean autoTrigger;
    private Style titleStyle;
    protected RealmList<Action> actions;
    protected String source;
```

## ONprint Image Database




Insert/Update:
``` java
 Image.insertOrUpdateInDatabase(image);
 ```

Delete:
``` java
 Image.delete(image);
 ```

Get an image from an ONprintImage ID:
``` java
 Image.getImageById(imageID)
 ```

Get all ONprintImage images:
``` java
 Image.getAll()
 ```


### > Image search

This function allows to treat Enriched Image callback:
``` java
 public EnrichedImage(File file, String contentType, String languageCode,
 HashMap<String, String> headers, IEnrichedImage callback);
 ```

Example: 
``` java
 new EnrichedImage(imageFile, "image/jpeg", FR-fr, null, new IEnrichedImage() {
 @Override
 public void enriched(JSONObject object) {
 // Pour parser image
 Image image = Parse.jsonImage(object);
 }

 @Override
 public void notEnriched() {
 // not Enriched
 }

 @Override
 public void failed(String error) {
 // error
 }
 });
 ```


### > Click


Here is the function allowing to manage click:
``` java
public Click(String sessionId, String actionId, IClick callback)
```

Example:

``` java
 new Click(sessionId, actionId, new IClick() {
 @Override
 public void succes() {
 Log.i(LOG_TAG, "SendClick [ OK ]");
 }

 @Override
 public void failed(String error) {
 Log.e(LOG_TAG, "SendClick [ ERROR ] \n" + error);
 }
 });
 ```



### > Layouts Customization


Set no boot splash screen :
``` java
SDKLayouts.no_splash_screen();
```

Set no tuorial launched at first start:
``` java
SDKLayouts.no_first_start_tuto();
```

Inside SplashScreen.java, we can see Camera customization:
``` java
 SDKLayouts.set_camera_layout(R.layout.custom_camera_view);
 ```
Here we're defining our layout for own layout camera design.

Note that layout's ID names have to be respected.

Custom the camera layout:
``` java
- public static void set_camera_layout (int cl)
- public static int get_camera_layout()
```

Custom the "Picture not recognized" layout:
``` java
- public static void set_no_result_dialog(Class cl, int no_result_layout, Activity a)
- public static Class get_no_result_dialog()
```

Custom actions list layout:
``` java
- public static void set_action_layout (int cl)
- public static int get_action_layout()
```

Cutom WebView layout:
``` java
- public static void set_web_layout (int cl)
- public static int get_web_layout()
```

Custom favorite view layout:
``` java
- public static void set_favorite_layout (int cl)
- public static int get_favorite_layout()
```

Custom tutorial 4 pages layout:
``` java
- public static void set_tutorial_layouts (int tut_1, int tut_2, int tut_3, int tut_4)
- public static int get_tuto_1()
- public static int get_tuto_2()
- public static int get_tuto_3()
- public static int get_tuto_4()
```

Activate animation on tutos:
``` java
- public static void active_anim()
- public static boolean is_with_anim()
```


[1]: https://gitlab.com/onprint_public/Android_ONprint_Light_SDK_Sample/tree/master/DEV.md

